package NLG;

import java.io.IOException;

import java.util.*;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import FSM.FSM;


/**
 * The NLG class collects and store entities&FSMs and calls FSM Library to update the correlate FSMs.
 * Call each FSMs can find the status of each entity
 * @author Lixiao Zhu
 *
 */
public class NLG {
	// A map stores the name of an entity as key
	// The map also stores each entity's status machine
	private Map<String,FSM> entityMap;
	// An ArrayList contain different entities
	private ArrayList<String> previousEntityClause_N_1;
	private ArrayList<String> previousEntityClause_N_2;

	


	/**
	 * NLG constructor. Initial data structures. 
	 */
	public NLG() {
		entityMap = new HashMap<String,FSM>();
		previousEntityClause_N_1 = new ArrayList<String>();
		previousEntityClause_N_2 = new ArrayList<String>();
	}
	
	/**
	 * Add clause. A clause will have two sets of entity. One is for focus(topic). One is for active(mentioned)
	 * @param topic
	 * @param mentioned
	 */
	public void addClause(Set<String> topic, Set<String> mentioned) {
		
		ArrayList<String> currentEntityClause = new ArrayList<String>();

		currentEntityClause.addAll(topic);
		currentEntityClause.addAll(mentioned);

		for(String entityName: previousEntityClause_N_1) {
			if(!currentEntityClause.contains(entityName)) {		
				updateEntityStatus(entityName,"NOT_MENTIONED");
			}
		}
		
		for(String entityName: previousEntityClause_N_2) {
			if(!currentEntityClause.contains(entityName) && !previousEntityClause_N_1.contains(entityName)) {		
				updateEntityStatus(entityName,"NOT_MENTIONED");
			}
		}
			
		for(String entity : topic) updateEntityStatus(entity,"TOPIC");
		
		for(String entity : mentioned) updateEntityStatus(entity,"MENTIONED");
		

		previousEntityClause_N_2.clear();
		previousEntityClause_N_2.addAll(previousEntityClause_N_1);
		
		previousEntityClause_N_1.clear();
		previousEntityClause_N_1.addAll(currentEntityClause);

	}
	
	/**
	 * Remove an entity from the entityMap and entityClause
	 * @param entityName : String
	 */
	public void removeEntity(String entityName) {
		previousEntityClause_N_1.remove(entityName);
		entityMap.remove(entityName);
	}
	
	/**
	 * Update the current entityMap by adding new entities
	 * @param entityName : String
	 * @param message : String
	 */
	public void updateEntityStatus(String entityName, String message) {
		// With input entity name and message, set up the clause and map
		//setClause(entityName);
		if(!entityMap.containsKey(entityName)) {
			try {
				// A new FSM for each entity
				FSM StatusMachine = new FSM();
				StatusMachine.ProcessFSM(message);
				entityMap.put(entityName, StatusMachine);
			}catch(IOException e) {
				System.out.println("Config File is not founded");
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				System.out.println("Error: ParserConfigurationException");
				e.printStackTrace();
			} catch (SAXException e) {
				System.out.println("Error: SAXException");
				e.printStackTrace();
			}
		}else {
			entityMap.get(entityName).ProcessFSM(message);
		}
	}
	
	/**
	 * Initialize the clause, which is a list of entity.
	 * @param entityName : String
	 */
	public void setClause(String entityName) {	
		previousEntityClause_N_1.add(entityName);
	}
	
	/**
	 * Print out the entityMap
	 */
	public void printEntityMap() {
		Set<String> keySet = entityMap.keySet();
		for(String entity:keySet) {
			System.out.print(entity);
			System.out.print(" is in " + entityMap.get(entity).getCurrentState() + "\n");
		}
	}
	
	/**
	 * After the user choose to end up the conversation. 
	 * Empty currentClause and entityMap.
	 */
	public void endConversation() {
		previousEntityClause_N_1.clear();
		entityMap.clear();
		System.out.println("End of the conversation.");
	}
	
	/**
	 * A getter method to get the current status for an entity
	 * @param entityName
	 * @return currentStatus : String
	 */
	public String getEntityStatus(String entityName) {
		return entityMap.get(entityName).getCurrentState();
		
	}
	
	/**
	 * A getter method to return the entityMap
	 * @return entityMap : Map<String, FSM>
	 */
	public Map<String, FSM> getEntityMap() {
		return entityMap;
	}

	/**
	 * A getter to return the current clause
	 * @return entityClause : ArrayList<String>
	 */
	public ArrayList<String> getEntityClause_N() {
		return previousEntityClause_N_1;	}
	
	/**
	 * A getter to return the previous clause
	 * @return entityClause : ArrayList<String>
	 */
	public ArrayList<String> getEntityClause_N_1() {
		return previousEntityClause_N_2;
	}
		
	
	
	
//	public static void main(String[] args) {
//		NLG nlg;
//		nlg = new NLG();
//		nlg.addEntity("Book", "TOPIC");
//		nlg.addEntity("Laptop", "MENTIONED");
//		nlg.addEntity("Iphone", "MENTIONED");
//		nlg.printEntityMap();
//		System.out.println();
//		nlg.updateEntityStatus("Book", "NOT_MENTIONED");
//		nlg.updateEntityStatus("Laptop", "NOT_MENTIONED");
//		nlg.updateEntityStatus("Iphone", "TOPIC");
//		nlg.addEntity("MacBook", "MENTIONED");
//		nlg.printEntityMap();
//		System.out.println();
//		nlg.updateEntityStatus("Book", "TOPIC");
//		nlg.printEntityMap();
//		nlg.endConversation();
//	}

}
