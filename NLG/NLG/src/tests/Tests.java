package tests;

import org.junit.*;
import NLG.NLG;
import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
/**
 * Multiple tests to test the correct functionality of NLG class.
 * @author Lixiao Zhu
 *
 */
public class Tests {
	
	private static NLG nlg;
	
	@Before
	public void setUp() {
		nlg = new NLG();
	}
	
	@Test
	public void testInitialStatus() {
		// Initial 'Book' from START to FOC
		nlg.updateEntityStatus("Book", "TOPIC");
		assertEquals("FOC",nlg.getEntityStatus("Book"));
		// Initial 'Laptop' from START to ACT
		nlg.updateEntityStatus("Laptop", "MENTIONED");
		assertEquals("ACT",nlg.getEntityStatus("Laptop"));
	}
	
	@Test
	public void testDowngradeStatus() {
		// Initial 'Book' and 'Laptop' to be in FOC
		nlg.updateEntityStatus("Book", "TOPIC");
		assertEquals("FOC",nlg.getEntityStatus("Book"));
		nlg.updateEntityStatus("Laptop", "TOPIC");
		assertEquals("FOC",nlg.getEntityStatus("Laptop"));
		
		// Downgrade 'Book' from FOC to ACT by message 'MENTIONED'
		nlg.updateEntityStatus("Book", "MENTIONED");
		assertEquals("ACT",nlg.getEntityStatus("Book"));
		// Downgrade 'Laptop' from FOC to ACT by message 'NOT_MENTIONED'
		nlg.updateEntityStatus("Laptop", "NOT_MENTIONED");
		assertEquals("ACT",nlg.getEntityStatus("Laptop"));
		
		// Downgrade 'Book' from ACT to FAM by message 'NOT_MENTIONED'
		nlg.updateEntityStatus("Book", "NOT_MENTIONED");
		assertEquals("FAM",nlg.getEntityStatus("Book"));

	}
	
	@Test
	public void testUpgradeStatus() {
		// Initial 'Book' and 'Laptop' to be in ACT
		// Then downgrade to FAM
		nlg.updateEntityStatus("Book", "MENTIONED");
		assertEquals("ACT",nlg.getEntityStatus("Book"));
		nlg.updateEntityStatus("Laptop", "MENTIONED");
		assertEquals("ACT",nlg.getEntityStatus("Laptop"));
		
		// Downgrade 'Book' from ACT to FAM by message 'NOT_MENTIONED'
		nlg.updateEntityStatus("Book", "NOT_MENTIONED");
		assertEquals("FAM",nlg.getEntityStatus("Book"));
		// Downgrade 'Laptop' from ACT to FAM by message 'NOT_MENTIONED'
		nlg.updateEntityStatus("Laptop", "NOT_MENTIONED");
		assertEquals("FAM",nlg.getEntityStatus("Laptop"));
		
		// Upgrade 'Book' from FAM to FOC by message 'TOPIC'
		nlg.updateEntityStatus("Book", "TOPIC");
		assertEquals("FOC",nlg.getEntityStatus("Book"));
		// Upgrade 'Laptop' from FAM to ACT by message 'MENTIONED'
		nlg.updateEntityStatus("Laptop", "MENTIONED");
		assertEquals("ACT",nlg.getEntityStatus("Laptop"));
		
		// Upgrade 'Laptop' from ACT to FOC by message 'TOPIC'
		nlg.updateEntityStatus("Laptop", "TOPIC");
		assertEquals("FOC",nlg.getEntityStatus("Laptop"));
		
	}
	
	@Test
	public void testInitialStatusClause() {
		String[] topicArray = {"Book", "Laptop", "Iphone"};
		String[] mentionedArray = {"Mines Markets", "Brown Hall", "Student Center"};

		Set<String> topic = new HashSet<String>();
		Set<String> mentioned = new HashSet<String>();
		
		for(String entityName : topicArray) {
			topic.add(entityName);
		}
		
		for(String entityName : mentionedArray) {
			mentioned.add(entityName);
		}
		
		nlg.addClause(topic, mentioned);
		// check if the entity is in the entityClause;
		assertTrue(nlg.getEntityClause_N().contains("Book"));
		assertTrue(nlg.getEntityClause_N().contains("Laptop"));
		assertTrue(nlg.getEntityClause_N().contains("Iphone"));
		assertTrue(nlg.getEntityClause_N().contains("Mines Markets"));
		assertTrue(nlg.getEntityClause_N().contains("Brown Hall"));
		assertTrue(nlg.getEntityClause_N().contains("Student Center"));
		
		// check if the entity is in the entityMap;
		assertTrue(nlg.getEntityMap().containsKey("Book"));
		assertTrue(nlg.getEntityMap().containsKey("Laptop"));
		assertTrue(nlg.getEntityMap().containsKey("Iphone"));
		assertTrue(nlg.getEntityMap().containsKey("Mines Markets"));
		assertTrue(nlg.getEntityMap().containsKey("Brown Hall"));
		assertTrue(nlg.getEntityMap().containsKey("Student Center"));
		
		// check the status of an entity
		assertEquals("FOC",nlg.getEntityStatus("Book"));
		assertEquals("FOC",nlg.getEntityStatus("Laptop"));
		assertEquals("FOC",nlg.getEntityStatus("Iphone"));
		assertEquals("ACT",nlg.getEntityStatus("Mines Markets"));
		assertEquals("ACT",nlg.getEntityStatus("Brown Hall"));
		assertEquals("ACT",nlg.getEntityStatus("Student Center"));

	}
	
	@Test
	public void testUpdateStatusClause() {
		String[] topicArray = {"Book", "Laptop", "Iphone"};
		String[] mentionedArray = {"Mines Markets", "Brown Hall", "Student Center"};

		Set<String> topic = new HashSet<String>();
		Set<String> mentioned = new HashSet<String>();
		
		// topic = {Book, Laptop, Iphone}
		for(String entityName : topicArray) {
			topic.add(entityName);
		}
		
		// mentioned = {Mines Markets, Brown Hall, Student Center}
		for(String entityName : mentionedArray) {
			mentioned.add(entityName);
		}

		nlg.addClause(topic, mentioned);

		// check if the entity is in the entityClause;
		assertTrue(nlg.getEntityClause_N().contains("Book"));
		assertTrue(nlg.getEntityClause_N().contains("Laptop"));
		assertTrue(nlg.getEntityClause_N().contains("Iphone"));
		assertTrue(nlg.getEntityClause_N().contains("Mines Markets"));
		assertTrue(nlg.getEntityClause_N().contains("Brown Hall"));
		assertTrue(nlg.getEntityClause_N().contains("Student Center"));
		
		// check if the entity is in the entityMap;
		assertTrue(nlg.getEntityMap().containsKey("Book"));
		assertTrue(nlg.getEntityMap().containsKey("Laptop"));
		assertTrue(nlg.getEntityMap().containsKey("Iphone"));
		assertTrue(nlg.getEntityMap().containsKey("Mines Markets"));
		assertTrue(nlg.getEntityMap().containsKey("Brown Hall"));
		assertTrue(nlg.getEntityMap().containsKey("Student Center"));
		
		// check the status of an entity
		assertEquals("FOC",nlg.getEntityStatus("Book"));
		assertEquals("FOC",nlg.getEntityStatus("Laptop"));
		assertEquals("FOC",nlg.getEntityStatus("Iphone"));
		assertEquals("ACT",nlg.getEntityStatus("Mines Markets"));
		assertEquals("ACT",nlg.getEntityStatus("Brown Hall"));
		assertEquals("ACT",nlg.getEntityStatus("Student Center"));

		//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		// topic = {Book,Laptop,Iphone,Mines Markets,Brown Hall,Student Center}
		topic.addAll(mentioned);
		mentioned.clear();
		// mentioned = {Table}
		mentioned.add("Table");
		
		nlg.addClause(topic, mentioned);
		// check if the entity is in the entityClause;
		assertTrue(nlg.getEntityClause_N().contains("Book"));
		assertTrue(nlg.getEntityClause_N().contains("Laptop"));
		assertTrue(nlg.getEntityClause_N().contains("Iphone"));
		assertTrue(nlg.getEntityClause_N().contains("Mines Markets"));
		assertTrue(nlg.getEntityClause_N().contains("Brown Hall"));
		assertTrue(nlg.getEntityClause_N().contains("Student Center"));
		assertTrue(nlg.getEntityClause_N().contains("Table"));

		
		// check if the entity is in the entityMap;
		assertTrue(nlg.getEntityMap().containsKey("Book"));
		assertTrue(nlg.getEntityMap().containsKey("Laptop"));
		assertTrue(nlg.getEntityMap().containsKey("Iphone"));
		assertTrue(nlg.getEntityMap().containsKey("Mines Markets"));
		assertTrue(nlg.getEntityMap().containsKey("Brown Hall"));
		assertTrue(nlg.getEntityMap().containsKey("Student Center"));
		assertTrue(nlg.getEntityMap().containsKey("Table"));

		// check the status of an entity
		assertEquals("FOC",nlg.getEntityStatus("Book"));				// Keep in the FOC
		assertEquals("FOC",nlg.getEntityStatus("Laptop"));
		assertEquals("FOC",nlg.getEntityStatus("Iphone"));
		assertEquals("FOC",nlg.getEntityStatus("Mines Markets"));		// Upgrade from ACT to FOC
		assertEquals("FOC",nlg.getEntityStatus("Brown Hall"));
		assertEquals("FOC",nlg.getEntityStatus("Student Center"));
		assertEquals("ACT",nlg.getEntityStatus("Table"));

		//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		// topic = {}
		topic.clear();
		mentioned.clear();
		// mentioned = {"Car"}
		mentioned.add("Car");
		for(String entity:topic) {
			System.out.print(entity + " ");
		}
		System.out.println();
		nlg.addClause(topic, mentioned);
		// check if the entity is in the entityClause;
//		assertTrue(nlg.getEntityClause_N_1().contains("Book"));
//		assertTrue(nlg.getEntityClause_N_1().contains("Laptop"));
//		assertTrue(nlg.getEntityClause_N_1().contains("Iphone"));
//		assertTrue(nlg.getEntityClause_N().contains("Mines Markets"));
//		assertTrue(nlg.getEntityClause_N().contains("Brown Hall"));
//		assertTrue(nlg.getEntityClause_N().contains("Student Center"));
//		assertTrue(nlg.getEntityClause_N().contains("Table"));
//		assertTrue(nlg.getEntityClause_N().contains("Car"));
		
		// check if the entity is in the entityMap;
		assertTrue(nlg.getEntityMap().containsKey("Book"));
		assertTrue(nlg.getEntityMap().containsKey("Laptop"));
		assertTrue(nlg.getEntityMap().containsKey("Iphone"));
		assertTrue(nlg.getEntityMap().containsKey("Mines Markets"));
		assertTrue(nlg.getEntityMap().containsKey("Brown Hall"));
		assertTrue(nlg.getEntityMap().containsKey("Student Center"));
		assertTrue(nlg.getEntityMap().containsKey("Table"));
		assertTrue(nlg.getEntityMap().containsKey("Car"));

		// check the status of an entity
		assertEquals("ACT",nlg.getEntityStatus("Book"));
		assertEquals("ACT",nlg.getEntityStatus("Laptop"));
		assertEquals("ACT",nlg.getEntityStatus("Iphone"));
		assertEquals("ACT",nlg.getEntityStatus("Mines Markets"));
		assertEquals("ACT",nlg.getEntityStatus("Brown Hall"));
		assertEquals("ACT",nlg.getEntityStatus("Student Center"));	// Downgrade from FOC to ACT
		assertEquals("FAM",nlg.getEntityStatus("Table"));			// Downgrade from ACT to FAM
		assertEquals("ACT",nlg.getEntityStatus("Car"));
		
		
		//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		// topic = {}
		topic.clear();
		mentioned.clear();
		// mentioned = {"Dog"}
		mentioned.add("Dog");
		for(String entity:topic) {
			System.out.print(entity + " ");
		}
		System.out.println();
		nlg.addClause(topic, mentioned);
		// check if the entity is in the entityClause;
//		assertTrue(nlg.getEntityClause_N().contains("Book"));
//		assertTrue(nlg.getEntityClause_N().contains("Laptop"));
//		assertTrue(nlg.getEntityClause_N().contains("Iphone"));
//		assertTrue(nlg.getEntityClause_N().contains("Mines Markets"));
//		assertTrue(nlg.getEntityClause_N().contains("Brown Hall"));
//		assertTrue(nlg.getEntityClause_N().contains("Student Center"));
//		assertTrue(nlg.getEntityClause_N().contains("Table"));
//		assertTrue(nlg.getEntityClause_N().contains("Car"));
//		assertTrue(nlg.getEntityClause_N().contains("Dog"));

		// check if the entity is in the entityMap;
		assertTrue(nlg.getEntityMap().containsKey("Book"));
		assertTrue(nlg.getEntityMap().containsKey("Laptop"));
		assertTrue(nlg.getEntityMap().containsKey("Iphone"));
		assertTrue(nlg.getEntityMap().containsKey("Mines Markets"));
		assertTrue(nlg.getEntityMap().containsKey("Brown Hall"));
		assertTrue(nlg.getEntityMap().containsKey("Student Center"));
		assertTrue(nlg.getEntityMap().containsKey("Table"));
		assertTrue(nlg.getEntityMap().containsKey("Car"));
		assertTrue(nlg.getEntityMap().containsKey("Dog"));


		// check the status of an entity
		assertEquals("FAM",nlg.getEntityStatus("Book"));
		assertEquals("FAM",nlg.getEntityStatus("Laptop"));
		assertEquals("FAM",nlg.getEntityStatus("Iphone"));
		assertEquals("FAM",nlg.getEntityStatus("Mines Markets"));
		assertEquals("FAM",nlg.getEntityStatus("Brown Hall"));
		assertEquals("FAM",nlg.getEntityStatus("Student Center"));	// Downgrade from ACT to FAM
		assertEquals("FAM",nlg.getEntityStatus("Table"));			// Stay same
		assertEquals("FAM",nlg.getEntityStatus("Car"));
		assertEquals("ACT",nlg.getEntityStatus("Dog"));
		
		//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		mentioned.clear();
		nlg.addClause(topic, mentioned);
		// check the status of an entity
		assertEquals("FAM",nlg.getEntityStatus("Book"));
		assertEquals("FAM",nlg.getEntityStatus("Laptop"));
		assertEquals("FAM",nlg.getEntityStatus("Iphone"));
		assertEquals("FAM",nlg.getEntityStatus("Mines Markets"));
		assertEquals("FAM",nlg.getEntityStatus("Brown Hall"));
		assertEquals("FAM",nlg.getEntityStatus("Student Center"));	// Stay in FAM
		assertEquals("FAM",nlg.getEntityStatus("Table"));			// Stay same
		assertEquals("FAM",nlg.getEntityStatus("Car"));
		assertEquals("FAM",nlg.getEntityStatus("Dog"));

	}
	
}

